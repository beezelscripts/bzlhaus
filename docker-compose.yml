version: "2.3"
services:
  portainer:
    image: portainer/portainer
    container_name: portainer
    restart: always
    command: --templates http://templates/templates.json
    ports:
      - "9000:9000"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ${USERDIR}/docker/portainer/data:/data
      - ${USERDIR}/docker/shared:/shared
    environment:
      - TZ=${TZ}
  organizr:
    container_name: organizr
    restart: always
    image: lsiocommunity/organizr
    volumes:
      - ${USERDIR}/docker/organizr:/config
      - ${USERDIR}/docker/shared:/shared
    ports:
      - "8079:80"
      - "8179:443"
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
      - TZ=${TZ}
  watchtower:
    container_name: watchtower
    restart: always
    image: v2tec/watchtower
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    command: --schedule "0 0 4 * * *" --cleanup
  sabnzbd:
    image: "linuxserver/sabnzbd"
    container_name: "sabnzbd"
    volumes:
      - ${USERDIR}/docker/sabnzbd:/config
      - ${USERDIR}/downloads/completed:/downloads
      - ${USERDIR}/downloads/incomplete:/incomplete-downloads
      - ${USERDIR}/docker/shared:/shared
      - ${USERDIR}/tv:/data/tv
      - ${USERDIR}/movies:/data/movies
      - ${USERDIR}/data:/data/data
    ports:
        - "8080:8080"
    restart: always
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
      - TZ=${TZ}
  plexms:
    container_name: plexms
    restart: always
    image: plexinc/pms-docker:plexpass
    runtime: nvidia
    volumes:
      - ${USERDIR}/docker/plex:/config
      - /dev/shm:/transcode
      - ${USERDIR}/movies:/movies
      - ${USERDIR}/tv:/tv
      - ${USERDIR}/docker/shared:/shared
      - ${USERDIR}/races:/races
    ports:
      - "32400:32400/tcp"
      - "3005:3005/tcp"
      - "8324:8324/tcp"
      - "32469:32469/tcp"
      - "1900:1900/udp"
      - "32410:32410/udp"
      - "32412:32412/udp"
      - "32413:32413/udp"
      - "32414:32414/udp"
    environment:
      - TZ=${TZ}
      - HOSTNAME=${PLEXHOSTNAME}
      - PLEX_CLAIM=${PLEX_CLAIMID}
      - PLEX_UID=${PUID}
      - PLEX_GID=${PGID}
      - ADVERTISE_IP=${PLEXIP}
      - NVIDIA_VISIBLE_DEVICES=ALL
      - NVIDIA_DRIVER_CAPABILITIES=compute,video,utility
  tautulli:
    container_name: tautulli
    restart: always
    image: linuxserver/tautulli
    volumes:
      - ${USERDIR}/docker/tautulli/config:/config
      - ${USERDIR}/docker/tautulli/logs:/logs:ro
      - ${USERDIR}/docker/shared:/shared
    ports:
      - "8181:8181"
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
      - TZ=${TZ}
  sonarr:
    container_name: sonarr
    restart: always
    image: linuxserver/sonarr:preview
    environment: 
      - PUID=${PUID}
      - PGID=${PGID}
      - TZ=${TZ}
    volumes:
      - ${USERDIR}/docker/sonarr/config:/config
      - ${USERDIR}/tv:/data/tv
      - ${USERDIR}/downloads:/data/downloads
    ports:
      - 8989:8989
  radarr:
    container_name: radarr
    restart: always
    image: linuxserver/radarr:preview
    environment: 
      - PUID=${PUID}
      - PGID=${PGID}
      - TZ=${TZ}
    volumes:
      - ${USERDIR}/docker/radarr/config:/config
      - ${USERDIR}/movies:/data/movies
      - ${USERDIR}/downloads:/data/downloads
    ports:
      - 7878:7878
  influxdb:
    image: influxdb:latest
    container_name: influxdb
    ports:
      - "8083:8083"
      - "8086:8086"
      - "8090:8090"
    env_file:
      - 'influxdb.env'
    volumes:
      - ${USERDIR}/docker/influxdb/data:/var/lib/influxdb
  chronograf:
    image: chronograf:latest
    container_name: chronograf
    volumes:
      - ${USERDIR}/docker/chronograf:/var/lib/chronograf
    ports:
      - "8888:8888"
    links:
      - influxdb
  telegraf:
    image: telegraf:latest
    container_name: telegraf
    hostname: bubbles
    links:
      - influxdb
    environment:
      - HOST_PROC=/rootfs/proc
      - HOST_SYS=/rootfs/sys
      - HOST_ETC=/rootfs/etc
    volumes:
      - ./telegraf.conf:/etc/telegraf/telegraf.conf:ro
      - /var/run/docker.sock:/var/run/docker.sock
      - /media:/media:ro
      - /sys:/rootfs/sys:ro
      - /proc:/rootfs/proc:ro
      - /etc:/rootfs/etc:ro
    restart: always
  grafana:
    image: grafana/grafana:latest
    container_name: grafana
    ports:
      - "3000:3000"
    env_file:
      - 'grafana.env'
    user: "0"
    links:
      - influxdb
    volumes:
      - ${USERDIR}/docker/grafana/data:/var/lib/grafana
  hddtemp:
    container_name: hddtemp
    image: drewster727/hddtemp-docker:latest
    restart: always
    privileged: true
    ports:
      - 7634:7634
    environment:
      - HDDTEMP_ARGS=-d -F /dev/sd*
      - TZ=${TZ}
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:rw
      - /dev:/dev:ro
  varken:
    image: boerderij/varken
    container_name: varken
    volumes:
      - ${USERDIR}/docker/varken/config:/config
    env_file: './varken.env'
  samba:
    image: dperson/samba
    container_name: samba
    restart: always
    ports:
      - "139:139"
      - "445:445"
    volumes:
      - ${USERDIR}:/mount
    command: ${SMBSHARE}
  homeassistant:
    container_name: homeassistant
    restart: unless-stopped
    image: homeassistant/home-assistant
    volumes:
      - ${USERDIR}/docker/homeassistant/config:/config
      - /etc/localtime:/etc/localtime:ro
      - ${USERDIR}/docker/shared:/shared
    network_mode: host
    privileged: true
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
      - TZ=${TZ}
networks:
  default:
    driver: bridge
